package tat.application.presentation;

import tat.application.businesslogic.ShapeManager;
import tat.application.domain.SphereShape;

import javax.swing.*;

/**
 * Created by Thommy.
 */
public class SphereForm extends JFrame {

    private JPanel panelSphere;
    private JTextField textFieldName;
    private JTextField textFieldRadius;
    private JButton okButton;
    private ShapeManager shapemanager;

    public SphereForm(ShapeManager manager) {

        super("New Sphere");
        shapemanager = manager;
        okButton.addActionListener(e -> pressOK());

        setContentPane(panelSphere);
    }

    private void pressOK(){

        try {
            SphereShape sphereShape = new SphereShape();

            sphereShape.setName(textFieldName.getText());
            sphereShape.setRadius(Double.parseDouble(textFieldRadius.getText()));

            shapemanager.addAbstractShape(sphereShape);
            dispose();

        }
        catch(Exception e)
        {

            JOptionPane.showMessageDialog(null, "Input is empty or not valid.", "Error", JOptionPane.PLAIN_MESSAGE);
        }


    }

}

