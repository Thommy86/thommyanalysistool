package tat.application.presentation;

import tat.application.businesslogic.ShapeManager;
import tat.application.domain.CubeShape;

import javax.swing.*;

/**
 * Created by Thommy.
 */
public class CubeForm  extends JFrame {
    private JPanel panelCube;
    private JTextField textFieldName;
    private JTextField textFieldHeight;
    private JTextField textFieldLength;
    private JTextField textFieldWidth;
    private JButton okButton;
    private ShapeManager shapemanager;

    public CubeForm(ShapeManager manager) {

        super("New Cube");
        shapemanager = manager;
        okButton.addActionListener(e -> pressOK());

        setContentPane(panelCube);
    }

    private void pressOK(){

        try {
            CubeShape cubeshape = new CubeShape();

            cubeshape.setName(textFieldName.getText());
            cubeshape.setWidth(Double.parseDouble(textFieldWidth.getText()));
            cubeshape.setLenght(Double.parseDouble(textFieldLength.getText()));
            cubeshape.setHeight(Double.parseDouble(textFieldHeight.getText()));

            shapemanager.addAbstractShape(cubeshape);
            dispose();

        }
        catch(Exception e)
        {

            JOptionPane.showMessageDialog(null, "Input is empty or not valid.", "Error", JOptionPane.PLAIN_MESSAGE);
        }


    }

}
