package tat.application.presentation;

import tat.application.businesslogic.FileDataController;
import tat.application.businesslogic.DatabaseDataController;
import tat.application.businesslogic.ShapeListUpdateListener;
import tat.application.businesslogic.ShapeManager;
import tat.application.domain.AbstractShape;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;

/**
 * Created by Thommy.
 */
public class MainForm extends JFrame {

    private JPanel mainPanel;
    private JPanel leftPanel;
    private JPanel rightPanel;
    private JButton ButtonDelete;
    private JButton rightButton;
    private JList listShapes;
    private JPanel topRight;
    private JPanel topLeft;
    private JTextField textVolume;
    private JTextField textTotalVolume;
    private JButton ButtonClearList;

    private ShapeManager shapeManager;
    private FileDataController fileDataController;
    private DatabaseDataController databaseDataController;
    private DefaultListModel ListModel = new DefaultListModel();

    public MainForm() {
        super("Thommy Analysis Tool");
        addMenu();
        this.setSize(800, 400);
        this.setVisible(true);
        this.setLocationRelativeTo(null);

        setContentPane(mainPanel);

        textVolume.setEnabled(false);
        textVolume.setDisabledTextColor(Color.black);
        textTotalVolume.setEnabled(false);
        textTotalVolume.setDisabledTextColor(Color.black);

        shapeManager = new ShapeManager();

        fileDataController = new FileDataController(shapeManager);
        databaseDataController = new DatabaseDataController(shapeManager);

        ShapeListUpdateListener listener = () -> UpdateList();
        shapeManager.addListener(listener);

        listShapes.setModel(ListModel);

        ListSelectionListener selectionListener = (selectionEvent) -> calculateSelected();
        listShapes.addListSelectionListener(selectionListener);

        ButtonDelete.addActionListener(e -> DeleteSelected());
        ButtonClearList.addActionListener(e -> DeleteAll());
    }

    private void addMenu() {
        //Add menu bar to our frame
        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu load = new JMenu("Load");
        JMenu save = new JMenu("Save");
        JMenu shape = new JMenu("Shapes");
        JMenu about = new JMenu("About");
        //now add menu items to these Menu objects

        load.add(new JMenuItem("Load TXT")).addActionListener(new WindowHandler());
        load.add(new JMenuItem("Load OBJ")).addActionListener(new WindowHandler());
        load.add(new JMenuItem("Load DB")).addActionListener(new WindowHandler());
        file.add(load);

        save.add(new JMenuItem("Save TXT")).addActionListener(new WindowHandler());
        save.add(new JMenuItem("Save OBJ")).addActionListener(new WindowHandler());
        save.add(new JMenuItem("Save DB")).addActionListener(new WindowHandler());
        file.add(save);

        file.add(new JMenuItem("Exit")).addActionListener(new WindowHandler());

        shape.add(new JMenuItem("CubeShape")).addActionListener(new WindowHandler());
        shape.add(new JMenuItem("CylinderShape")).addActionListener(new WindowHandler());
        shape.add(new JMenuItem("SphereShape")).addActionListener(new WindowHandler());
        shape.add(new JMenuItem("PyramidShape")).addActionListener(new WindowHandler());
        shape.add(new JMenuItem("ConeShape")).addActionListener(new WindowHandler());

        about.add(new JMenuItem("About")).addActionListener(new WindowHandler());

        //add menus to menubar
        menuBar.add(file);
        menuBar.add(shape);
        menuBar.add(about);

        //menuBar.setVisible(true);
        if (null == this.getJMenuBar()) {
            this.setJMenuBar(menuBar);
        }
    }//addMenu

    //windowHandler events
    private class WindowHandler extends WindowAdapter implements ActionListener {
  
        public void actionPerformed(ActionEvent e) {

            switch (e.getActionCommand()) {
                case "Exit":
                    System.exit(0);
                    break;
                case "Load TXT":
                    ImportTxt();
                    break;
                case "Load OBJ":
                    ImportXML();
                    break;
                case "Load DB":
                    ImportDatabase();
                    break;
                case "Save TXT":
                    GenerateTxt();
                    JOptionPane.showMessageDialog(null, "Shapes saved as a TXT file.", "Saved TXT", JOptionPane.PLAIN_MESSAGE);
                    break;
                case "Save OBJ":
                    GenerateXML();
                    JOptionPane.showMessageDialog(null, "Shapes saved as an object.", "Saved object", JOptionPane.PLAIN_MESSAGE);
                    break;
                case "Save DB":
                    GenerateDatabase();
                    JOptionPane.showMessageDialog(null, "Shapes saved in a database.", "Save database", JOptionPane.PLAIN_MESSAGE);
                    break;
                case "CubeShape":
                    CubeForm cubeForm = new CubeForm(shapeManager);
                    cubeForm.setSize(320, 250);
                    cubeForm.setVisible(true);
                    cubeForm.setLocationRelativeTo(null);
                    break;
                case "CylinderShape":
                    CylinderForm cylinderForm = new CylinderForm(shapeManager);
                    cylinderForm.setSize(320, 250);
                    cylinderForm.setVisible(true);
                    cylinderForm.setLocationRelativeTo(null);
                    break;
                case "SphereShape":
                    SphereForm sphereForm = new SphereForm(shapeManager);
                    sphereForm.setSize(320, 250);
                    sphereForm.setVisible(true);
                    sphereForm.setLocationRelativeTo(null);
                    break;
                case "PyramidShape":
                    PyramidForm pyramidForm = new PyramidForm(shapeManager);
                    pyramidForm.setSize(320, 250);
                    pyramidForm.setVisible(true);
                    pyramidForm.setLocationRelativeTo(null);
                    break;
                case "ConeShape":
                    ConeForm coneForm = new ConeForm(shapeManager);
                    coneForm.setSize(320, 250);
                    coneForm.setVisible(true);
                    coneForm.setLocationRelativeTo(null);
                    break;
                case "About":
                    JOptionPane.showMessageDialog(null, "This is a small program written in Java for Avans VAT casus by Thommy Radstaat.", "About", JOptionPane.PLAIN_MESSAGE);
                    break;
                default:
                    break;
            }

        }//actionPerformed()

    }//windowHandler


    public void UpdateList() {

        ListModel.removeAllElements();
        for (AbstractShape shape : shapeManager.getAbstractShapeList()) {
            ListModel.addElement(shape);
        }

        CalculateTotal();
    }

    public void DeleteSelected() {
        try {
            int selectedIndex = listShapes.getSelectedIndex();

            AbstractShape selectedShape = (AbstractShape) listShapes.getSelectedValue();

            if (selectedIndex < 0) {
                return;
            }

            ListModel.remove(selectedIndex);

            shapeManager.deleteAbstractShape(selectedShape);

            CalculateTotal();
        } catch (Exception e) {
            String temp = e.getMessage();
            JOptionPane.showMessageDialog(null, "error in array.", "Logger", JOptionPane.PLAIN_MESSAGE);

        }
    }

    public void DeleteAll() {
        try {
            ListModel.clear();
            shapeManager.clearListShapes();
            CalculateTotal();
        } catch (Exception e) {
            String temp = e.getMessage();
            JOptionPane.showMessageDialog(null, "error in array.", "Logger", JOptionPane.PLAIN_MESSAGE);

        }
    }

    /**
     * Format double to string
     *
     * @param volume as double
     * @return volume in string
     */
    private String formatDoubleToString(double volume) {
        return String.format("%.2f", volume);
    }

    private void calculateSelected() {
        int selectedIndex = listShapes.getSelectedIndex();

        if (selectedIndex < 0) {
            textVolume.setText(null);
            return;
        }
        AbstractShape selectedShape = (AbstractShape) listShapes.getSelectedValue();
        textVolume.setText(formatDoubleToString(selectedShape.calculateVolume()));
    }

    private void CalculateTotal() {
        double total = 0;

        for (AbstractShape shape : shapeManager.getAbstractShapeList()) {
            total += shape.calculateVolume();
        }

        textTotalVolume.setText(formatDoubleToString(total));
    }

    private void GenerateXML() {
        fileDataController.GenerateXML(shapeManager.getAbstractShapeList());
    }

    private void GenerateTxt() {
        fileDataController.GenerateTxt(shapeManager.getAbstractShapeList());
    }

    private void ImportXML() {
        fileDataController.ImportXML();
    }

    private void ImportTxt() {
        fileDataController.ImportTxt();
    }

    //DB
    private void ImportDatabase() {
        databaseDataController.ImportDatabase();
    }

    private void GenerateDatabase() {
        databaseDataController.GenerateDatabase(shapeManager.getAbstractShapeList());
    }
}