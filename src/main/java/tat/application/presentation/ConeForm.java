package tat.application.presentation;

import tat.application.businesslogic.ShapeManager;
import tat.application.domain.ConeShape;

import javax.swing.*;

/**
 * Created by Thommy.
 */
public class ConeForm extends JFrame {

    private JPanel panelCone;
    private JTextField textFieldName;
    private JTextField textFieldHeight;
    private JTextField textFieldRadius;
    private JButton okButton;
    private ShapeManager shapemanager;

    public ConeForm(ShapeManager manager) {
        super("New Cone");
        shapemanager = manager;
        okButton.addActionListener(e -> pressOK());

        setContentPane(panelCone);
    }

    private void pressOK(){

        try {
            ConeShape coneShape = new ConeShape();

            coneShape.setName(textFieldName.getText());
            coneShape.setHeight(Double.parseDouble(textFieldHeight.getText()));
            coneShape.setRadius(Double.parseDouble(textFieldRadius.getText()));

            shapemanager.addAbstractShape(coneShape);
            dispose();

        }
        catch(Exception e)
        {

            JOptionPane.showMessageDialog(null, "Input is empty or not valid.", "Error", JOptionPane.PLAIN_MESSAGE);
        }


    }

}