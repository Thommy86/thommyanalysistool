package tat.application.presentation;

import tat.application.businesslogic.ShapeManager;
import tat.application.domain.CylinderShape;

import javax.swing.*;

/**
 * Created by Thommy.
 */
public class CylinderForm extends JFrame{

    private JPanel panelCylinder;
    private JTextField textFieldName;
    private JTextField textFieldHeight;
    private JTextField textFieldRadius;
    private JButton okButton;
    private ShapeManager shapemanager;

    public CylinderForm(ShapeManager manager) {
        super("New Cylinder");
        shapemanager = manager;
        okButton.addActionListener(e -> pressOK());

    setContentPane(panelCylinder);
}

    private void pressOK(){

        try {
            CylinderShape cylinderShape = new CylinderShape();

            cylinderShape.setName(textFieldName.getText());
            cylinderShape.setHeight(Double.parseDouble(textFieldHeight.getText()));
            cylinderShape.setRadius(Double.parseDouble(textFieldRadius.getText()));

            shapemanager.addAbstractShape(cylinderShape);
            dispose();

        }
        catch(Exception e)
        {

            JOptionPane.showMessageDialog(null, "Input is empty or not valid.", "Error", JOptionPane.PLAIN_MESSAGE);
        }


    }

}