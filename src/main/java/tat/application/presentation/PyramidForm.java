package tat.application.presentation;

import tat.application.businesslogic.ShapeManager;
import tat.application.domain.PyramidShape;

import javax.swing.*;

/**
 * Created by Thommy.
 */

public class PyramidForm  extends JFrame {
    private JPanel panelPyramid;
    private JTextField textFieldName;
    private JTextField textFieldHeight;
    private JTextField textFieldLength;
    private JTextField textFieldWidth;
    private JButton okButton;
    private ShapeManager shapemanager;

    public PyramidForm(ShapeManager manager) {

        super("New Pyramid");
        shapemanager = manager;
        okButton.addActionListener(e -> pressOK());

        setContentPane(panelPyramid);
    }

    private void pressOK(){

        try {
            PyramidShape pyramidShape = new PyramidShape();

            pyramidShape.setName(textFieldName.getText());
            pyramidShape.setWidth(Double.parseDouble(textFieldWidth.getText()));
            pyramidShape.setLenght(Double.parseDouble(textFieldLength.getText()));
            pyramidShape.setHeight(Double.parseDouble(textFieldHeight.getText()));

            shapemanager.addAbstractShape(pyramidShape);
            dispose();

        }
        catch(Exception e)
        {

            JOptionPane.showMessageDialog(null, "Input is empty or not valid.", "Error", JOptionPane.PLAIN_MESSAGE);
        }


    }

}
