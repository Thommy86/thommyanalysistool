package tat.application.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thommy.
 */
public class CubeShape extends AbstractShape {

    private double height = 0;
    private double length = 0;
    private double width = 0;

    /**
     * Get height of the cube
     *
     * @return height of the cube
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets height of the cube
     *
     * @param value height of cube
     */
    public void setHeight(double value) {
        this.height = value;
    }

    /**
     * Get lenght of the cube
     *
     * @return lenght of the cube
     */
    public double getLenght() {
        return length;
    }

    /**
     * Sets lenght of the cube
     *
     * @param value lenght of cube
     */
    public void setLenght(double value) {
        this.length = value;
    }

    /**
     * Get width of the cube
     *
     * @return width of the cube
     */
    public double getWidth() {
        return width;
    }

    /**
     * Sets width of the cube
     *
     * @param value width of cube
     */
    public void setWidth(double value) {
        this.width = value;
    }
    /**
     * Set Type of the cube
     *
     * @return Type of the cube
     */
    public CubeShape()
    {
        setType(Type.Cube);
    }
    /**
     * Calculates Volume of the shape
     *
     * @return Volume of the shape
     */
    @Override
    public double calculateVolume() {
        return height * length * width;
    }

    /**
     * @return String with name and values od the shape
     */
    @Override
    public String toString()
    {
        return this.getName() + " - " + this.getType() +  " = H: " + this.getHeight() + ", L: " + this.getLenght() + ", W: " + this.getWidth();
    }

}

