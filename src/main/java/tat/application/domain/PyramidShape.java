package tat.application.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thommy.
 */
public class PyramidShape extends AbstractShape{

    private double height = 0;
    private double length = 0;
    private double width = 0;

    /**
     * Get height of the pyramid
     *
     * @return height of the pyramid
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets height of the pyramid
     *
     * @param value height of pyramid
     */
    public void setHeight(double value) {
        this.height = value;
    }

    /**
     * Get lenght of the pyramid
     *
     * @return lenght of the pyramid
     */
    public double getLenght() {
        return length;
    }

    /**
     * Sets lenght of the pyramid
     *
     * @param value lenght of pyramid
     */
    public void setLenght(double value) {
        this.length = value;
    }

    /**
     * Get width of the pyramid
     *
     * @return width of the pyramid
     */
    public double getWidth() {
        return width;
    }

    /**
     * Sets width of the pyramid
     *
     * @param value width of pyramid
     */
    public void setWidth(double value) {
        this.width = value;
    }
    /**
     * Set Type of the Pyramid
     *
     * @return Type of the Pyramid
     */
    public PyramidShape()
    {
        setType(Type.Pyramid);
    }
    /**
     * Calculates Volume of the shape
     *
     * @return Volume of the shape
     */
    @Override
    public double calculateVolume() {
        return height * length * width / 3;
    }

    /**
     * @return String with name and values od the shape
     */
    @Override
    public String toString()
    {
        return this.getName() + " - " + this.getType() +  " = H: " + this.getHeight() + ", L: " + this.getLenght() + ", W: " + this.getWidth();
    }

}

