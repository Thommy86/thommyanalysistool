package tat.application.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thommy.
 */
public class SphereShape extends AbstractShape{

    private double radius;

    /**
     * Get radius of the sphere
     *
     * @return radius of the sphere
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Sets radius of the sphere
     *
     * @param radius of sphere
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }
    /**
     * Set Type of the Sphere
     *
     * @return Type of the Sphere
     */
    public SphereShape()
    {
        setType(Type.Sphere);
    }

    /**
     * Calculates Volume of the shape
     *
     * @return Volume of the shape
     */
    @Override
    public double calculateVolume() {
        return (4d / 3d) * Math.pow(radius, 3);
    }

    /**
     * @return String with name and values od the shape
     */
    @Override
    public String toString()
    {
        return this.getName() + " - " + this.getType() +  " = R: " + this.getRadius();
    }
}
