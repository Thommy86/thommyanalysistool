package tat.application.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thommy.
 */
public class ConeShape extends AbstractShape{

    /**
     * Constants
     */

    private double height;
    private double radius;


    /**
     * Get height of the cone
     *
     * @return height of the cone
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets height of the cone
     *
     * @param height of cone
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Get radius of the cone
     *
     * @return radius of the cone
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Sets radius of the cone
     *
     * @param radius of cone
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * Sets Type of the cone
     */
    public ConeShape()
    {
        setType(Type.Cone);
    }

    /**
     * Calculates Volume of the shape
     *
     * @return Volume of the shape
     */
    @Override
    public double calculateVolume() {
        return (1. / 3.) * Math.PI * Math.pow(radius, 2.0) * height;
    }


    /**
     * @return String with name and values od the shape
     */
    @Override
    public String toString()
    {
        return this.getName() + " - " + this.getType() +  " = H: " + this.getHeight() + ", R: " + this.getRadius();
    }
}
