package tat.application.domain;

import java.util.Map;

/**
 * Created by Thommy.
 */
public interface Shape {

    /**
     * Get name of the shape
     *
     * @return shape name
     */
    String getName();

    /**
     * Set name of the shape
     *
     * @param name of the shape
     */
    void setName(String name);

    /**
     * Get volume of the shape
     *
     * @return volume of shape
     */
    double calculateVolume();

}

