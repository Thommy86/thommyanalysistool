package tat.application.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thommy.
 */
public class CylinderShape extends AbstractShape{

    /**
     * Constants
     */

    private double height;
    private double radius;


    /**
     * Get height of the cylinder
     *
     * @return height of the cylinder
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets height of the cylinder
     *
     * @param height of cylinder
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Get radius of the cylinder
     *
     * @return radius of the cylinder
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Sets radius of the cylinder
     *
     * @param radius of cylinder
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }
    /**
     * Set Type of the Cylinder
     *
     * @return Type of the Cylinder
     */
    public CylinderShape()
    {
        setType(Type.Cylinder);
    }
    /**
     * Calculates Volume of the shape
     *
     * @return Volume of the shape
     */
    @Override
    public double calculateVolume() {
        return Math.PI * Math.pow(radius, 2) * height;
    }

    /**
     * @return String with name and values od the shape
     */
    @Override
    public String toString()
    {
        return this.getName() + " - " + this.getType() +  " = H: " + this.getHeight() + ", R: " + this.getRadius();
    }
}
