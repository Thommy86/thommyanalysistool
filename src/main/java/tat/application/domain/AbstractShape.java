package tat.application.domain;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Thommy.
 */
public abstract class AbstractShape implements Shape, Serializable {

    private String name;
    private Type type;

    public enum Type {
        Cone,
        Cube,
        Cylinder,
        Pyramid,
        Sphere
    }

    /**
     * Get name of the shape
     *
     * @return name of the shape
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets name of the shape
     *
     * @param name of the shape
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get Type of the shape
     *
     * @return Type of the shape
     */
    public Type getType()
    {
        return type;
    }
    /**
     * Set Type of the shape
     *
     * @return Type of the shape
     */
    public void setType(Type type)
    {
        this.type = type;
    }

    /**
     * Calculates Volume of the shape
     *
     * @return Volume of the shape
     */
    @Override
    public abstract double calculateVolume();
}
