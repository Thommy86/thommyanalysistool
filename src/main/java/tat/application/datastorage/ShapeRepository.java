package tat.application.datastorage;

import tat.application.domain.*;

import java.util.ArrayList;
import java.util.logging.*;
import java.sql.*;
import java.util.Locale;


/**
 * Created by Thommy.
 */
public class ShapeRepository {

    private DatabaseConnection dbconnection;

    private String create = "insert into shapes (type, name, width, height, radius, length) VALUES (%s, %s, %d, %d, %d, %d)";
    private String read =   "select * from shapes where id = %1;";
    private String readAllQuery =   "select * from shapes;";
  //  private string update = "update  from shapes where id = %1;";
    private String delete = "delete from shapes where id = %1;";

    public ShapeRepository()
    {
        dbconnection = new DatabaseConnection();
    }

    public ArrayList<AbstractShape> getAllShapes()
    {
        ArrayList<AbstractShape> allShapes = new ArrayList<AbstractShape>();

        ResultSet resultSet = dbconnection.executeQuery(readAllQuery);

        try {
            while (resultSet.next()) {
                String type = resultSet.getString("type");
                String name = resultSet.getString("name");
                Double width = resultSet.getDouble("width");
                Double height = resultSet.getDouble("height");
                Double radius = resultSet.getDouble("radius");
                Double length = resultSet.getDouble("length");

                switch (AbstractShape.Type.valueOf(type)) {
                    case Cone:
                        ConeShape cone = new ConeShape();
                        cone.setName(name);
                        cone.setRadius(radius);
                        cone.setHeight(height);
                        allShapes.add(cone);
                        break;
                    case Cube:
                        CubeShape cube = new CubeShape();
                        cube.setName(name);
                        cube.setLenght(length);
                        cube.setWidth(width);
                        cube.setHeight(height);
                        allShapes.add(cube);
                        break;
                    case Cylinder:
                        CylinderShape cylinder = new CylinderShape();
                        cylinder.setName(name);
                        cylinder.setRadius(radius);
                        cylinder.setHeight(height);
                        allShapes.add(cylinder);
                        break;
                    case Sphere:
                        SphereShape sphere = new SphereShape();
                        sphere.setName(name);
                        sphere.setRadius(radius);
                        allShapes.add(sphere);
                        break;
                    case Pyramid:
                        PyramidShape pyramid = new PyramidShape();
                        pyramid.setName(name);
                        pyramid.setLenght(length);
                        pyramid.setWidth(width);
                        pyramid.setHeight(height);
                        allShapes.add(pyramid);
                        break;
                }

            }
        } catch (Exception e) {
            System.out.println("Error getAllShapes");
            return new ArrayList<>();
        } finally {
            dbconnection.closeConnection();
        }

        return allShapes;
    }

    public void create(AbstractShape shape)
    {

        Locale.setDefault(new Locale("en", "US"));

        try {
            switch (shape.getType()) {
                case Cone:
                    ConeShape cone = (ConeShape) shape;
                    String createCone = "insert into shapes (type, name, height, radius) VALUES ('%s', '%s', %f, %f)";
                    String insertConeStatement = String.format(createCone, cone.getType(), cone.getName(), cone.getHeight(), cone.getRadius());

                    dbconnection.executeUpdate(insertConeStatement);

                    break;
                case Cube:
                    CubeShape cube = (CubeShape) shape;
                    String createCube = "insert into shapes (type, name, width, height, length) VALUES ('%s', '%s', %f, %f, %f)";
                    String insertCubeStatement = String.format(createCube, cube.getType(), cube.getName(), cube.getWidth(),cube.getHeight(), cube.getLenght());

                    dbconnection.executeUpdate(insertCubeStatement);
                    break;
                case Cylinder:
                    CylinderShape cylinder = (CylinderShape) shape;
                    String createCylinder = "insert into shapes (type, name, width, radius, height) VALUES ('%s', '%s', %f, %f,)";
                    String insertCylinderStatement = String.format(createCylinder, cylinder.getType(), cylinder.getName(), cylinder.getRadius(),cylinder.getHeight());

                    dbconnection.executeUpdate(insertCylinderStatement);
                    break;
                case Sphere:
                    SphereShape sphere = (SphereShape) shape;
                    String createSphere = "insert into shapes (type, name, radius) VALUES ('%s', '%s', %f,)";
                    String insertSphereStatement = String.format(createSphere, sphere.getType(), sphere.getName(), sphere.getRadius());

                    dbconnection.executeUpdate(insertSphereStatement);
                    break;
                case Pyramid:
                    PyramidShape pyramid = (PyramidShape) shape;
                    String createPyramid = "insert into shapes (type, name, width, height, length) VALUES ('%s', '%s', %f, %f, %f)";
                    String insertPyramidStatement = String.format(createPyramid, pyramid.getType(), pyramid.getName(), pyramid.getWidth(),pyramid.getHeight(), pyramid.getLenght());

                    dbconnection.executeUpdate(insertPyramidStatement);
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error create database");
        } finally {
            dbconnection.closeConnection();
        }
    }
}
