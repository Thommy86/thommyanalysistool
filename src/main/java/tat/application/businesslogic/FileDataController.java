package tat.application.businesslogic;


import tat.application.datastorage.ShapeRepository;
import tat.application.domain.AbstractShape;

import java.beans.XMLDecoder;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thommy.
 */
public class FileDataController {

    private ShapeManager shapeManager;
    private ShapeRepository shapeRepository = new ShapeRepository();

    public FileDataController(ShapeManager manager)
    {
        this.shapeManager = manager;
    }

    public void GenerateXML(List<AbstractShape> list)
    {
        try {
            createDirectoryIfNotExist("files/");
            FileOutputStream fos1 = new FileOutputStream("files/shapes.xml");
            java.beans.XMLEncoder xe1 = new java.beans.XMLEncoder(fos1);
            xe1.writeObject(list);
            xe1.flush();
            xe1.close();
        }
        catch(Exception e)
        {
            System.out.println("Error GenerateXML");
        }
    }

    public void ImportXML()
    {
        try {

            XMLDecoder d = new XMLDecoder(
                    new BufferedInputStream(
                            new FileInputStream("files/shapes.xml")));

            ArrayList<AbstractShape> list = (ArrayList<AbstractShape>)d.readObject();

            for(AbstractShape shape : list)
            {
                shapeManager.addAbstractShape(shape);
            }

            d.close();
        }
        catch(Exception e)
        {
            System.out.println("Error ImportXML");
        }
    }

    public void GenerateTxt(List<AbstractShape> list)
    {
        try
        {
            createDirectoryIfNotExist("files/");
            FileOutputStream fos= new FileOutputStream("files/shapes.dat");
            ObjectOutputStream oos= new ObjectOutputStream(fos);
            oos.writeObject(list);
            oos.close();
            fos.close();

        }
        catch (Exception e) {
            System.out.println("Error GenerateTxt");
        }
    }


    public void ImportTxt()
    {
        try {
            FileInputStream fis = new FileInputStream("files/shapes.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<AbstractShape> list = (ArrayList<AbstractShape>) ois.readObject();

            for(AbstractShape shape : list)
            {
                shapeManager.addAbstractShape(shape);
            }
            ois.close();
            fis.close();

        }
        catch(Exception e)
        {
            System.out.println("Error ImportTxt");
        }
    }

    /**
     * Creates a directory if is not exist
     */
    public boolean createDirectoryIfNotExist(String filePath) {
        File directory = new File(String.valueOf(filePath));

        return !directory.exists() && directory.mkdir();
    }
}
