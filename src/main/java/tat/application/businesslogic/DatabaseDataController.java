package tat.application.businesslogic;


import tat.application.datastorage.ShapeRepository;
import tat.application.domain.AbstractShape;

import java.beans.XMLDecoder;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thommy.
 */
public class DatabaseDataController {

    private ShapeManager shapeManager;
    private ShapeRepository shapeRepository = new ShapeRepository();

    public DatabaseDataController(ShapeManager manager) {
        this.shapeManager = manager;
    }

    public void ImportDatabase() {
        shapeManager.clearListShapes();

        ShapeRepository shapeRepo = new ShapeRepository();

        for (AbstractShape shape : shapeRepo.getAllShapes()) {
            shapeManager.addAbstractShape(shape);
        }
    }

    public void GenerateDatabase(List<AbstractShape> list) {
        ShapeRepository shapeRepo = new ShapeRepository();

        for (AbstractShape shape : list) {
            shapeRepo.create(shape);
        }
    }
}