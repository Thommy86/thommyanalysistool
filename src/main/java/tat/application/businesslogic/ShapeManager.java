package tat.application.businesslogic;

import tat.application.datastorage.ShapeRepository;
import tat.application.domain.AbstractShape;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thommy.
 */
/*interface ShapeListUpdateListener {
    void UpdateShapeList();
}*/

public class ShapeManager {

    private ArrayList<AbstractShape> abstractList = new ArrayList<>();

    public ArrayList<AbstractShape> getAbstractShapeList()
    {
        return abstractList;
    }

    public ShapeManager()
    {

    }

    public void addAbstractShape(AbstractShape shape)
    {
        abstractList.add(shape);
        OnShapesAdded();
    }

    public void deleteAbstractShape(AbstractShape shape)
    {
        abstractList.remove(shape);
    }

    public void clearListShapes ()
    {
        abstractList.clear();
    }

     public void ImportDatabase() {
         clearListShapes();

         ShapeRepository shapeRepo =new ShapeRepository();

         for (AbstractShape shape : shapeRepo.getAllShapes()) {
            abstractList.add(shape);
        }

         OnShapesAdded();
     }

    public List<ShapeListUpdateListener> listeners = new ArrayList<ShapeListUpdateListener>();

    public void addListener(ShapeListUpdateListener toAdd)
    {
        listeners.add(toAdd);
    }

    public void OnShapesAdded()
    {
        for (ShapeListUpdateListener slul : listeners)
            slul.UpdateShapeList();
    }

}
